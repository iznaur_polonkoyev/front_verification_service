import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'channel'
})
export class ChannelPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (value === 'OPEN_PORTAL') {
      return 'Открытый портал';
    }
    return 'Неизвестный портал';
  }

}
