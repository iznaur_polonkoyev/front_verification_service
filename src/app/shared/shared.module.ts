import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NzButtonModule,
  NzCollapseModule,
  NzGridModule,
  NzLayoutModule,
  NzModalModule,
  NzSpinModule
} from 'ng-zorro-antd';
import {DateFormatPipe} from '../pipes/date-format.pipe';
import {AlarmStatusPipe} from '../pipes/alarm-status.pipe';
import {IgnoreNullPipe} from '../pipes/ignore-null.pipe';
import {ChannelPipe} from '../pipes/channel.pipe';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {GrassFieldPipe} from '../pipes/grass-field.pipe';
import {ApprovedPipe} from '../pipes/approved.pipe';

@NgModule({
  declarations: [
    DateFormatPipe,
    AlarmStatusPipe,
    IgnoreNullPipe,
    GrassFieldPipe,
    ChannelPipe,
    ApprovedPipe
  ],
  exports: [
    DateFormatPipe,
    AlarmStatusPipe,
    GrassFieldPipe,
    ApprovedPipe,
    IgnoreNullPipe,
    ChannelPipe
  ],
  imports: [
    CommonModule,
    LeafletModule,
    NzSpinModule,
    NzGridModule,
    NzButtonModule,
    NzModalModule,
    NzLayoutModule,
    NzCollapseModule,
  ]
})
export class SharedModule {
}
