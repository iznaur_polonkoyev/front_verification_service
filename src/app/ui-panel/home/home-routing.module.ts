import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';

const preffix = '../home';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'checking',
        loadChildren: `${preffix}/pages/checking-request/checking-request.module#CheckingRequestModule`
      },
      {
        path: 'submit',
        loadChildren: `${preffix}/pages/submit-request/submit-request.module#SubmitRequestModule`
      },
      {
        path: '',
        redirectTo: 'submit',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
