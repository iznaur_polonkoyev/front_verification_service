import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {AlarmService} from '../../../../services/alarm.service';
import {OK, VALID} from '../../../../models/Constant';
import {ActivatedRoute, Router} from '@angular/router';
import {Alarm} from '../../../../models/Alarm';
import {HeaderTextService} from '../../../../services/header-text.service';
import {LocationService} from '../../../../services/location.service';
import {MSubmitComponent} from './m-submit/m-submit.component';

@Component({
  selector: 'app-submit-request',
  templateUrl: './submit-request.component.html',
  styleUrls: ['./submit-request.component.scss']
})
export class SubmitRequestComponent implements OnInit {

  validateForm: FormGroup;
  isLoader: any = false;
  region: string;
  regionkz: string;
  city: any;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private fb: FormBuilder,
              private modalService: NzModalService,
              private route: ActivatedRoute,
              private router: Router,
              private location: LocationService,
              private headerTextService: HeaderTextService,
              private notification: NzNotificationService,
              private alarmService: AlarmService) {
    this.city = this.location.getCitySettings();
    this.region = this.city?.region;
    this.regionkz = this.city?.region_kz;
    this.headerTextService.setMessage('Новая заявка');

    this.validateForm = this.fb.group({
      email: [null, [Validators.email]],
      name: [null, [Validators.required]],
      surname: [null, [Validators.required]],
      patronymic: [null, []],
      products: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {

  }

  submitForm(): void {
    // tslint:disable-next-line:forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.status === VALID) {
      this.isLoader = true;
      setTimeout(() => {
        const form = this.validateForm.getRawValue();
        form.products = [{name: 'Mercedes', status: 'NEW'}];
        this.alarmService.createPerson(form).then(response => {
          console.log(response);
          this.notification.success('Заявка отправлена', '');
          this.modal(response);
        }).catch(() => {
          this.notification.error('Ошибка при отправке заявки', '');
        }).finally(() => {
          this.isLoader = false;
        });
      }, 800);
    }
  }

  modal(activate) {
    const res = this.modalService.create({
      nzTitle: 'Активация',
      nzContent: MSubmitComponent,
      nzWidth: 700,
      nzOkText: 'Отправить',
      nzCancelText: 'Отменить',
      nzComponentParams: {activate}
    });

    res.afterClose.subscribe(resp => {
      if (resp) {
        console.log(resp);
      }
    });
  }

  resolved($event: string) {
    if ($event) {
      this.validateForm.patchValue({captcha: true});
    }
  }
}


