import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubmitRequestComponent} from './submit-request.component';
import {SubmitRequestRoutingModule} from './submit-request-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {RecaptchaModule} from 'ng-recaptcha';
import {TextMaskModule} from 'angular2-text-mask';
import {SharedModule} from '../../../../shared/shared.module';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {
  NzButtonModule,
  NzFormModule,
  NzGridModule,
  NzInputModule, NzModalControlService, NzModalModule,
  NzModalService,
  NzSelectModule,
  NzSpinModule,
  NzTypographyModule
} from 'ng-zorro-antd';
import {MSubmitComponent} from './m-submit/m-submit.component';

@NgModule({
  declarations: [SubmitRequestComponent, MSubmitComponent],
  imports: [
    CommonModule,
    SubmitRequestRoutingModule,
    ReactiveFormsModule,
    RecaptchaModule,
    TextMaskModule,
    SharedModule,
    LeafletModule,
    NzGridModule,
    NzTypographyModule,
    NzFormModule,
    NzInputModule,
    NzSelectModule,
    NzSpinModule,
    NzButtonModule,
    NzModalModule,
  ],
  providers: [NzModalService, NzModalControlService]
})
export class SubmitRequestModule {
}
