export const environment = {
  production: true,
  addressSearchByCoordinatesUrl: 'https://nominatim.openstreetmap.org/reverse?format=json&limit=100',
  initZoom: 14,
  url: 'http://164.90.230.207:8080'

};
